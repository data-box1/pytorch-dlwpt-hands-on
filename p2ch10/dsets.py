import copy
import csv
import functools
import glob
import os

from collections import namedtuple

import SimpleITK as sitk
import numpy as np

import torch
import torch.cuda
from torch.utils.data import Dataset

from util.disk import getCache
from util.util import XyzTuple, xyz2irc
from util.logconf import logging

log = logging.getLogger(__name__)
# log.setLevel(logging.WARN)
# log.setLevel(logging.INFO)
log.setLevel(logging.DEBUG)
# folder name for cahed files
raw_cache = getCache('part2ch10_raw')

#https://rico-schmidt.name/pymotw-3/collections/namedtuple.html
# Define tuple by named key instead of by index
CandidateInfoTuple = namedtuple(
    'CandidateInfoTuple',
    'isNodule_bool, diameter_mm, series_uid, center_xyz',
)
# Standard library in-memory caching
@functools.lru_cache(1)
def getCandidateInfoList(lunaDBPath, requireOnDisk_bool=True):
    '''
    Book's note:
    We construct a set with all series_uids that are present on disk.
    This will let us use the data, even if we haven't downloaded all of
    the subsets yet.
    '''
    # mhd files path
    mhd_list = glob.glob(lunaDBPath+'/subset*/*.mhd')
    
    presentOnDisk_set = {os.path.split(p)[-1][:-4] for p in mhd_list}
    # Note: The annotations.csv file contains information about some of the candidates that
    # have been flagged as nodules
    diameter_dict = {}
    with open(lunaDBPath+'/CSVFILES/annotations.csv', "r") as f:
        for row in list(csv.reader(f))[1:]:
            series_uid = row[0]
            annotationCenter_xyz = tuple([float(x) for x in row[1:4]])
            annotationDiameter_mm = float(row[4])

            diameter_dict.setdefault(series_uid, []).append(
                (annotationCenter_xyz, annotationDiameter_mm)
            )

    candidateInfo_list = []
    # Note: The candidates.csv file contains information about all lumps that potentially look like
    # nodules, whether those lumps are malignant, benign tumors, or something else alto-
    # gether
    with open(lunaDBPath+'/CSVFILES/candidates.csv', "r") as f:
        for row in list(csv.reader(f))[1:]:
            series_uid = row[0]

            if series_uid not in presentOnDisk_set and requireOnDisk_bool:
                continue

            isNodule_bool = bool(int(row[4]))
            candidateCenter_xyz = tuple([float(x) for x in row[1:4]])

            candidateDiameter_mm = 0.0
            for annotation_tup in diameter_dict.get(series_uid, []):
                annotationCenter_xyz, annotationDiameter_mm = annotation_tup
                for i in range(3):
                    delta_mm = abs(candidateCenter_xyz[i] - annotationCenter_xyz[i])
                    # Note:  Check that candidateCenter_xyz is not outside a 
                    # nodule bounding box
                    if delta_mm > annotationDiameter_mm / 4:
                        break
                    else:
                        candidateDiameter_mm = annotationDiameter_mm
                        break

            candidateInfo_list.append(CandidateInfoTuple(
                isNodule_bool,
                candidateDiameter_mm,
                series_uid,
                candidateCenter_xyz,
            ))
    # Reverse to get largest nodules first
    candidateInfo_list.sort(reverse=True)
    return candidateInfo_list

class Ct:
    def __init__(self, lunaDBPath, series_uid):
        mhd_path = glob.glob(
            lunaDBPath+'/subset*/{}.mhd'.format(series_uid)
        )[0]

        ct_mhd = sitk.ReadImage(mhd_path)
        ct_a = np.array(sitk.GetArrayFromImage(ct_mhd), dtype=np.float32)

        # Note: CTs are natively expressed in https://en.wikipedia.org/wiki/Hounsfield_scale
        # HU are scaled oddly, with 0 g/cc (air, approximately) being -1000 and 1 g/cc (water) being 0.
        # The lower bound gets rid of negative density stuff used to indicate out-of-FOV
        # The upper bound nukes any weird hotspots and clamps bone down
        ct_a.clip(-1000, 1000, ct_a)

        self.series_uid = series_uid
        self.hu_a = ct_a


        self.origin_xyz = XyzTuple(*ct_mhd.GetOrigin())
        self.vxSize_xyz = XyzTuple(*ct_mhd.GetSpacing())
        #print(ct_mhd.GetDirection())
        # reshape as a 3x3 matrix
        self.direction_a = np.array(ct_mhd.GetDirection()).reshape(3, 3)
        #print(self.direction_a)

    def getRawCandidate(self, center_xyz, width_irc):
        '''
        Book's note:
        Takes the center expressed in the patient coordinate system (X,Y,Z), just as it’s 
        specified in the LUNA CSV data, as well as a width in voxels. It returns a cubic
        chunk of CT, as well as the center of the candidate converted to array coordinates.
        '''
        center_irc = xyz2irc(
            center_xyz,
            self.origin_xyz,
            self.vxSize_xyz,
            self.direction_a,
        )

        slice_list = []
        for axis, center_val in enumerate(center_irc):
            start_ndx = int(round(center_val - width_irc[axis]/2))
            end_ndx = int(start_ndx + width_irc[axis])

            assert center_val >= 0 and center_val < self.hu_a.shape[axis], repr([self.series_uid, center_xyz, self.origin_xyz, self.vxSize_xyz, center_irc, axis])

            if start_ndx < 0:
                # log.warning("Crop outside of CT array: {} {}, center:{} shape:{} width:{}".format(
                #     self.series_uid, center_xyz, center_irc, self.hu_a.shape, width_irc))
                start_ndx = 0
                end_ndx = int(width_irc[axis])

            if end_ndx > self.hu_a.shape[axis]:
                # log.warning("Crop outside of CT array: {} {}, center:{} shape:{} width:{}".format(
                #     self.series_uid, center_xyz, center_irc, self.hu_a.shape, width_irc))
                end_ndx = self.hu_a.shape[axis]
                start_ndx = int(self.hu_a.shape[axis] - width_irc[axis])

            slice_list.append(slice(start_ndx, end_ndx))

        ct_chunk = self.hu_a[tuple(slice_list)]

        return ct_chunk, center_irc

'''
Book's notes: 
we’re caching the getCt return value in memory so that we can repeatedly ask for
the same Ct instance without having to reload all of the data from disk. That’s 
a huge speed increase in the case of repeated requests, but we’re only keeping
one CT in memory, so cache misses will be frequent if we’re not careful about
access order
'''
@functools.lru_cache(1, typed=True)
def getCt(lunaDBPath, series_uid):
    return Ct(lunaDBPath, series_uid)

'''
Book's notes:
The getCtRawCandidate function that calls getCt also has its outputs cached, how-
ever; so after our cache is populated, getCt won’t ever be called. These values are
cached to disk using the Python library diskcache .
'''
@raw_cache.memoize(typed=True)
def getCtRawCandidate(lunaDBPath, series_uid, center_xyz, width_irc):
    ct = getCt(lunaDBPath, series_uid)
    ct_chunk, center_irc = ct.getRawCandidate(center_xyz, width_irc)
    return ct_chunk, center_irc

class LunaDataset(Dataset):
    def __init__(self,
                 lunaDBPath,
                 val_stride=0,
                 isValSet_bool=None,
                 series_uid=None,
            ):
        # Copies the return value so the altering self.candidateInfo_list
        # Consistent sorted list    
        self.candidateInfo_list = copy.copy(getCandidateInfoList(lunaDBPath))
        self.lunaDBPath = lunaDBPath
        print('LunaDataset: ', len(self.candidateInfo_list))
        if series_uid:
            self.candidateInfo_list = [
                x for x in self.candidateInfo_list if x.series_uid == series_uid
            ]

        # Is train or val set?
        # If val_stride=0 no dataset separation occurs
        if isValSet_bool:
            # For validation set
            assert val_stride > 0, val_stride
            self.candidateInfo_list = self.candidateInfo_list[::val_stride]
            # Returns only validation candiates
            assert self.candidateInfo_list
        elif val_stride > 0:
        # For training set & val_stride > 0
        #Deletes the validation images. Copy made earlier so that we don’t alter the original list.
            del self.candidateInfo_list[::val_stride] #at each val_stride
            # Returns only trainning candiates, eliminating validation ones
            assert self.candidateInfo_list

        log.info("{!r}: {} {} samples".format(
            self,
            len(self.candidateInfo_list),
            "validation" if isValSet_bool else "training",
        ))

    def __len__(self):
        return len(self.candidateInfo_list)

    def __getitem__(self, ndx):
        candidateInfo_tup = self.candidateInfo_list[ndx]
        # depth, height, and width
        width_irc = (32, 48, 48)
        candidate_a, center_irc = getCtRawCandidate(
            self.lunaDBPath,
            candidateInfo_tup.series_uid,
            candidateInfo_tup.center_xyz,
            width_irc,
        )
        # Adapt to pytorch
        candidate_t = torch.from_numpy(candidate_a)
        candidate_t = candidate_t.to(torch.float32)
        # add a "Channel" dimension
        candidate_t = candidate_t.unsqueeze(0)
        # Classification tensor: This has two elements, one each for our possible candidate classes
        pos_t = torch.tensor([
                not candidateInfo_tup.isNodule_bool,
                candidateInfo_tup.isNodule_bool
            ],
            dtype=torch.long,
        )

        return (
            candidate_t,
            pos_t,
            candidateInfo_tup.series_uid,
            torch.tensor(center_irc),
        )
